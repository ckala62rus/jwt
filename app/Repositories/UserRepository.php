<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class UserRepository extends Repository
{
    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->model = new User();
    }

    /**
     * @return Builder
     */
    public function getQuery(): Builder
    {
        return $this
            ->model
            ->newQuery();
    }

    /**
     * Get records by phone and secret code
     * @param Builder $query
     * @param string $phone
     * @param string $code
     * @return Builder
     */
    public function getRecordByPhoneAndCode(Builder $query, string $phone, string $code): Builder
    {
        return $query
            ->where('phone', $phone)
            ->where('code', $code);
    }
}
