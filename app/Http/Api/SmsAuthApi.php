<?php

namespace App\Http\Api;

use App\Repositories\UserRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class SmsAuthApi
{

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * SmsAuthApi constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * Return string secret code from auth login user
     * @param string $phone
     * @return Model
     */
    public function getSmsCodeByPhoneNumber(string $phone): Model
    {
        //todo сделать запрос на апи для получения кода
        //todo так же сделать запись кода в бд и отправку пользователю

        return $this->userRepository->store([
            'name' => $phone,
            'phone' => $phone,
            'code' => Str::random(8),
            'password' => Hash::make(Str::random(10)),
        ]);
    }
}
