<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{

    /**
     * Get all posts by paginate
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $limit = $request->get('limit');

        $posts = Post::query()
            ->paginate($limit ?? 10);

        return response()->json([
            'data' => $posts,
            'count' => $posts->total()
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Create one post
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->all();

        $post = Post::create([
            'title' => $data['title'],
            'text' => $data['text'],
        ]);

        return response()->json($post, JsonResponse::HTTP_CREATED);
    }

    /**
     * Get one post by id or 404 error
     * @param $id
     * @return JsonResponse
     */
    public function show($id): JsonResponse
    {
        $post = Post::findOrfail($id);

        return response()->json($post, JsonResponse::HTTP_OK);
    }

    /**
     * Update one post by id
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id): JsonResponse
    {
        $data = $request->all();

        $postId = Post::query()->update($data);

        $post = Post::query()->where('id', $postId)->first();

        return response()->json($post, JsonResponse::HTTP_OK);
    }

    /**
     * Destroy one post by id
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $isDelete = Post::destroy($id);

        return response()->json($isDelete, JsonResponse::HTTP_OK);
    }

    public function quill(Request $request): JsonResponse
    {
        $file = $request->file;
        $file_name = $this->createFilename($file);

        $userId = 1;

        $storagePath = public_path() . '/upload/' . $userId . '/';

        $file->move($storagePath, $file_name);

        return response()->json([
            'path' => 'http://jwt/upload/' . $userId . '/' . $file_name,
            'original' => $file->getClientOriginalName()
        ]);
    }

    /**
     * Create unique filename for uploaded file
     * @param UploadedFile $file
     * @return string
     */
    protected function createFilename(UploadedFile $file)
    {
        return md5($file->getClientOriginalName() . time()) . '.' . $file->getClientOriginalExtension();
    }
}
