<?php

namespace App\Http\Controllers;

use App\Http\Api\SmsAuthApi;
use App\Http\Requests\Sms\AuthLoginRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;

class SmsAuthController extends Controller
{
    /**
     * @var SmsAuthApi|SmsAuthController
     */
    protected SmsAuthController|SmsAuthApi $smsApi;

    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * SmsAuthController constructor.
     * @param SmsAuthApi $authController
     * @param UserRepository $userRepository
     */
    public function __construct(
        SmsAuthApi $authController,
        UserRepository $userRepository
    ) {
        $this->smsApi = $authController;
        $this->userRepository = $userRepository;
    }

    /**
     * Get auth token by telephone number
     * @param AuthLoginRequest $request
     * @return JsonResponse
     */
    public function getTelephoneSecretCode(AuthLoginRequest $request): JsonResponse
    {
        $data = $request->all();

        $model = $this
            ->smsApi
            ->getSmsCodeByPhoneNumber($data['phone']);

        //todo после генерации кода и отправки его юзеру, возвращаем ответ, что все хорощо
        return response()->json($model, JsonResponse::HTTP_OK);
    }

    /**
     * Get auth token
     * @param Request $request
     * @return JsonResponse
     */
    public function getTokeBySmsCode(Request $request): JsonResponse
    {
        $data = $request->all();

        $query = $this
            ->userRepository
            ->getQuery();

        $user = $this
            ->userRepository
            ->getRecordByPhoneAndCode($query, $data['phone'], $data['code'])
            ->firstOrFail();

        $token = JWTAuth::fromUser($user);
        JWTAuth::setToken($token);

        if (!$user || ! $token) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        $this->userRepository->update(['verify' => 1], $user->id);

        if ($user) {
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ], JsonResponse::HTTP_OK);
        }

        return response()->json([], JsonResponse::HTTP_NOT_FOUND);
    }
}
